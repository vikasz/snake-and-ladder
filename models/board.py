from models.snake import Snake
from models.ladder import Ladder


class Board:
    def __init__(self, start: int = 1, finish: int = 100, name: str = "My Board"):
        self._name = ""
        self._start = 0
        self._finish = 0

        self.name = name
        self.start = start
        self.finish = finish
        self.range = range(self.start, self.finish + 1)

        self.snakes = []
        self.ladders = []

    # `name`
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = str(new_name)

    # `start`
    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, new_start):
        if not isinstance(new_start, int):
            raise TypeError(f"`start` must be of type `int`.")
        if not new_start >= 1:
            raise ValueError(f"`start` must be greater than or equal to `1`.")
        self._start = new_start

    # `finish`
    @property
    def finish(self):
        return self._finish

    @finish.setter
    def finish(self, new_finish):
        if not isinstance(new_finish, int):
            raise TypeError(f"`finish` must be of type `int`.")
        if not new_finish > self.start:
            raise ValueError(f"`finish` must be greater than `start`.")
        self._finish = new_finish

    def add_snake(self, snake: Snake):
        if not isinstance(snake, Snake):
            raise TypeError(f"`snake` must be an instance of the class `Snake`.")
        if not (snake.head < self.finish and snake.head > self.start):
            raise ValueError(f"`snake` head must fit into the `board` positions.")
        if not (snake.tail < self.finish and snake.tail >= self.start):
            raise ValueError(f"`snake` tail must fit into the `board` positions.")
        self.snakes.append(snake)

    def add_ladder(self, ladder: Ladder):
        if not isinstance(ladder, Ladder):
            raise TypeError(f"`ladder` must be an instance of the class `Ladder`.")
        if not (ladder.head <= self.finish and ladder.head > self.start):
            raise ValueError(f"`ladder` head must fit into the `board` positions.")
        if not (ladder.tail < self.finish and ladder.tail >= self.start):
            raise ValueError(f"`ladder` head must fit into the `board` positions.")
        self.ladders.append(ladder)

    # String representation of the instance.
    def __repr__(self):
        return (
            f"\n"
            + f"Board:"
            + f"\n\tName: {self.name}"
            + f"\n\tStart: {self.start}"
            + f"\n\tFinish: {self.finish}"
            + f"\n\tSnakes: {len(self.snakes)}"
            + f"\n\tLadders: {len(self.ladders)}"
            + f"\n"
        )
