from random import randint


class Dice:
    def __init__(self, min_score: int = 0, max_score: int = 6):
        self._min_score = 0
        self._max_score = 6

        self.min_score = min_score
        self.max_score = max_score

    # `min_score`
    @property
    def min_score(self):
        return self._min_score

    @min_score.setter
    def min_score(self, new_min_score):
        if not isinstance(new_min_score, int):
            raise TypeError(f"`min_score` must be of type `int`.")
        self._min_score = new_min_score

    # `max_score`
    @property
    def max_score(self):
        return self._max_score

    @max_score.setter
    def max_score(self, new_max_score):
        if not isinstance(new_max_score, int):
            raise TypeError(f"`max_score` must be of type `int`.")
        if not new_max_score > self.min_score:
            raise ValueError(f"`max_score` must be greater than `min_score`.")
        self._max_score = new_max_score

    def __repr__(self):
        return f"Dice: [{self.min_score} - {self.max_score}]"

    def roll(self):
        score = randint(self.min_score, self.max_score)
        print(f"Dice rolled...{score}!")
        return score
