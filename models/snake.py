class Snake:
    def __init__(self, head: int, tail: int, name: str = ""):
        self._name = ""
        self._head = 0
        self._tail = 0

        self.name = name
        self.head = head
        self.tail = tail

    # `name`
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        self._name = str(new_name)

    # `head`
    @property
    def head(self):
        return self._head

    @head.setter
    def head(self, new_head):
        if not isinstance(new_head, int):
            raise TypeError("`head` must be of type `int`.")
        self._head = new_head

    # `tail`
    @property
    def tail(self):
        return self._tail

    @tail.setter
    def tail(self, new_tail):
        if not isinstance(new_tail, int):
            raise TypeError("`tail` must be of type `int`.")
        if not new_tail < self.head:
            raise ValueError(f"`tail` must be lesser than the `head`.")
        self._tail = new_tail

    def __repr__(self):
        return (
            f"\n"
            + f"Snake:"
            + f"\n\tName: {self.name}"
            + f"\n\tHead: {self.head}"
            + f"\n\tTail: {self.tail}"
            + "\n"
        )
