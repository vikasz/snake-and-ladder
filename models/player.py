class Player:
    def __init__(self, name: str = ""):
        self._name = ""
        self._position = 0

        self.name = name

    # `name`
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, new_name):
        try:
            self._name = str(new_name)
        except ValueError:
            raise ValueError(f"Invalid type for `name`.")

    # `position`
    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, new_position: int):
        if not isinstance(new_position, int):
            raise TypeError(f"`position` must be of type `int`.")
        self._position = new_position

    # String rep.
    def __repr__(self):
        return (
            f"\n"
            + f"Player:"
            + f"\n\tName: {self.name}"
            + f"\n\tCurrent Position: {self.position}"
            + f"\n"
        )
