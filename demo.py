from models import board, snake, ladder, player, dice
import game

if __name__ == "__main__":
    s1 = snake.Snake(name="Snake 1", head=20, tail=10)
    s2 = snake.Snake(name="Snake 2", head=98, tail=80)
    s3 = snake.Snake(name="Snake 3", head=51, tail=43)

    l1 = ladder.Ladder(name="Ladder 1", head=78, tail=43)
    l2 = ladder.Ladder(name="Ladder 2", head=13, tail=3)
    l3 = ladder.Ladder(name="Ladder 3", head=100, tail=95)

    b = board.Board()

    [b.add_snake(s) for s in (s1, s2, s3)]

    [b.add_ladder(l) for l in (l1, l2, l3)]

    d = dice.Dice(1, 6)

    p1 = player.Player(name="Player 1")
    p2 = player.Player(name="Player 2")

    g = game.Game(board=b, dice=d)
    g.add_player(p1)
    g.add_player(p2)

    g.start()
