from models.board import Board
from models.snake import Snake
from models.ladder import Ladder
from models.player import Player
from models.dice import Dice

from time import sleep


class Game:
    def __init__(self, board: Board, dice: Dice):
        self._board = None
        self._dice = None
        self._players = []

        self.start_time = 0
        self.end_time = 0

        self.winner = None

        self.board = board
        self.dice = dice

    # `board`
    @property
    def board(self):
        return self._board

    @board.setter
    def board(self, new_board: Board):
        if not isinstance(new_board, Board):
            raise TypeError(f"`board` must be an instance of the class `Board`.")
        self._board = new_board

    # `dice`
    @property
    def dice(self):
        return self._dice

    @dice.setter
    def dice(self, new_dice: Dice):
        if not isinstance(new_dice, Dice):
            raise TypeError(f"`dice` must be an instance of the class `Dice`.")
        self._dice = new_dice

    # `players`
    @property
    def players(self):
        return self._players

    def add_player(self, new_player: Player):
        if not isinstance(new_player, Player):
            raise TypeError(f"`player` must be an instance of the class `Player`.")
        new_player.position = self.board.start
        self._players.append(new_player)

    def __repr__(self):
        return (
            f"\n"
            + f"\nGame:"
            + f"\nBoard: {self.board}"
            + f"\nDice: {self.dice}"
            + f"\nPlayers: {self.players}"
            + f"\n"
        )

    def _move(self, player: Player, new_position: int):
        if not isinstance(player, Player):
            raise TypeError(f"`player` must be an instance of the class `Player`.")
        if not isinstance(new_position, int):
            raise TypeError(f"`new_position` must be of type `int`.")
        if not new_position in self.board.range:
            raise ValueError(f"`new_position` must fit into the `Board` positions.")
        for snake in self.board.snakes:
            if new_position == snake.head:
                new_position = snake.tail
                print(f"Arrgh. Snake bite!")
        for ladder in self.board.ladders:
            if new_position == ladder.tail:
                new_position = ladder.head
                print(f"A ladder. Cool!")
        new_position += player.position
        if new_position <= self.board.finish:
            player.position = new_position
        return True

    def start(self):
        while not self.winner:
            for id, player in enumerate(self.players, 1):
                print(f"\n" + f"Player {id}: {player.name}")
                self._move(player=player, new_position=self.dice.roll())
                print(f"Player moved to {player.position}")
                sleep(1)
                if player.position == self.board.finish:
                    print("\nWON !!!\n")
                    self.winner = player
                    break
        print(f"The winner is: {self.winner}")
        print(f"Congratulations!!!")
